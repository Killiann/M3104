<?php

use M3104\consultation\ConsultationManager;
use M3104\medecin\MedecinManager;
use M3104\patient\PatientManager;

require './layout/middleware.php';

if (!isset($_GET['ID'])) { // Si le lien n'est pas valide on envoie l'utilisateur vers la page principale
    header('Location: ./index');
    return;
}

// Variables pour les messages d'erreur / les affichages de modification
$error = false;
$consultationError = false;
$consultationSuccess = false;
$modifyConsultation = false;
$modifyConsultationError = false;

// On vérifie qu'il n'y a pas d'injection ou tentative de faire crash le site
if (!is_numeric($id = $_GET['ID'])) {
    $error = true;
}

$patient = PatientManager::getInstance()->getAdapter()->get($id);
if (is_null($patient)) { // Si le l'id du patient n'est pas valide on affiche une erreur
    $error = true;
}

$manager = ConsultationManager::getInstance();
if (!$error) {

    //Quand l'utilisateur appuit sur le boutton créer pour créer une consultation
    if (isset($_POST['submit'])) {
        try {
            $consultation = $manager->createConsultation($id);
            $consultationSuccess = true;
        } catch (Exception $e) { // Si exception on affiche une erreur
            $consultationError = true;
        }
    } // Quand l'utilisateur appuit sur le bouron supprimer pour supprimer une consultation
    elseif (isset($_POST['delete'])) {
        if (is_numeric($idCon = $_POST['IDC'])) {
            $manager->getAdapter()->delete($idCon);
        }
    } // Quand l'utilisateur appuit sur le boutton modifier pour modifier l'usager
    elseif (isset($_POST['applyUs'])) {
        try {
            $patient = PatientManager::getInstance()->updatePatient($id);
        } catch (Exception $e) { // Si exception on affiche l'erreur
            echo $e;
        }
    } // Quand l'utilisateut appuit sur le bouton supprimer l'usager
    elseif (isset($_POST['deleteUS'])) {
        PatientManager::getInstance()->getAdapter()->delete($id);
        header('Location: ./index'); // On le renvoie vers la page principale
        exit(0);
    }
    //CONSULTATION

    // Si l'utilisateur appuit sur le bouton modifier => Pour remplacer les texte par des input
    elseif (isset($_POST['modify'])) {
        if (is_numeric($idCon = $_POST['IDC'])) {
            $modifyConsultation = true;
        }
    } // Quand l'utilisateur appuit sur appliquer pour modifier l'usager
    elseif (isset($_POST['applyCon'])) {
        if (is_numeric($idCon = $_POST['IDAC'])) {
            try {
                $manager->updateConsultation($id, $idCon);
            } catch (Exception $e) { // Si exception on affiche une erreur
                $modifyConsultationError = true;
            }
        }

    }

    // Récupère la liste des consultations d'un patient
    $consultations = $manager->getAdapter()->getFromPatient($id);
}

// Récupère la liste de tous les médecins
$medecins = MedecinManager::getInstance()->getAdapter()->getAll();

function hasSelectedConsultation(array $consultations): bool
{
    foreach ($consultations as $consultation) {
        if (($_GET["idCon"] ?? -1) == $consultation->getId()) {
            return true;
        }
    }
    return false;
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fiche usager</title>
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>
<body>
<?php require './layout/header.php'; ?>

<!-- Contenue de la page -->
<main>
    <div class="container">
        <div class="back content">
            <h1 class="bold minispace">FICHE USAGER</h1>

            <?php if (!$error): ?>
                <div class="minispace">
                    <fieldset>
                        <legend><h3> Information Usager </h3></legend>
                        <form method="POST">

                            <?php if (isset($_POST['modifyUs'])): ?>
                                <!-- Champ civilite -->
                                <label>
                                    <select name="civilite">
                                        <option value="M" <?= $patient->getCivility() == 'M' ? 'selected' : '' ?>>
                                            Monsieur
                                        </option>
                                        <option value="F" <?= $patient->getCivility() == 'F' ? 'selected' : '' ?>>Madame
                                        </option>
                                    </select>
                                </label>

                                <!-- Champ Nom -->
                                <div class="br">
                                    <label for="nom">Nom</label>
                                </div>
                                <input type="text" id="nom" name="nom" value="<?= $patient->getLastName() ?>" required>

                                <!-- Champ Prenom -->
                                <label for="prenom">Prenom</label>
                                <input type="text" id="prenom" name="prenom" value="<?= $patient->getName() ?>"
                                       required>

                                <!-- Champ date Naissance -->
                                <label for="dateNassaince"> Date de naissance : </label>
                                <input type="date" id="dateNassaince" name="dateNassaince"
                                       value="<?= date("Y-m-d", $patient->getBirthDate()) ?>">

                                <!-- Champ adresse -->
                                <div class="br">
                                    <label for="adresse">Adresse</label>
                                </div>
                                <input type="text" id="adresse" name="adresse" value="<?= $patient->getLocation() ?>"
                                       required>

                                <!-- Champ lieu naissance -->
                                <label for="lieuNaissance">Lieu de naissance</label>
                                <input type="text" id="lieuNaissance" name="lieuNaissance"
                                       value="<?= $patient->getBirthLocation() ?>" required>

                                <!-- Champ NumSecu -->
                                <label for="NumSecu">Numéro de sécurité social (15 chiffres)</label>
                                <input type="text" id="NumSecu" name="NumSecu"
                                       value="<?= $patient->getSecurityNumber() ?>"
                                       required>

                                <!-- Champ medecin -->
                                <label for="medecin">Médecin traitant</label>
                                <select id="medecin" name="medecin">
                                    <option value="-1">Aucun</option>
                                    <?php foreach ($medecins as $medecin): ?>
                                        <option value="<?= $medecin->getId() ?>" <?= $medecin->getId() == $patient->getMedecinId() ? ' selected' : '' ?>>
                                            <?= $medecin ?></option>
                                    <?php endforeach; ?>
                                </select>

                                <input type="submit" id="applyUs" name="applyUs" value="Appliquer">
                            <?php else: ?>
                                <!-- Champ ID -->
                                <p><strong>Prenom : </strong></p>
                                <p><?= $patient->getName() ?></p>

                                <!-- Champ ID -->
                                <p><strong>Nom : </strong></p>
                                <p><?= $patient->getLastName() ?></p>

                                <!-- Champ Nom -->
                                <p><strong>Adresse : </strong></p>
                                <p><?= $patient->getLocation() ?></p>

                                <!-- Champ date Naissance -->
                                <p><strong>Date de Naissance : </strong></p>
                                <p><?= date("d/m/Y", $patient->getBirthDate()) ?></p>

                                <!-- Champ lieu naissance -->
                                <p><strong>Lieu de naissance : </strong></p>
                                <p><?= $patient->getBirthLocation() ?></p>

                                <!-- Champ NumSecu -->
                                <p class="hoverNumSecu"><strong>Numéro sécurité social (Mettre la souris sur ce texte
                                        pour
                                        l'afficher): </strong></p>
                                <p class="numSecu"><?= $patient->getSecurityNumber() ?></p>

                                <p><strong>Médecin traitant : </strong></p>
                                <p><?= MedecinManager::getInstance()->getAdapter()->get($patient->getMedecinId()) ?? 'Aucun' ?></p>

                                <input type="submit" id="modifyUs" name="modifyUs" value="Modifier">
                            <?php endif; ?>
                        </form>

                    </fieldset>
                </div>

                <div class="minispace">
                    <fieldset>
                        <legend><h3> Créer une Consultation </h3></legend>
                        <form method="POST">
                            <!-- Select de la Civilité -->
                            <label for="medecin">Medecin </label>
                            <select id="medecin" name="medecin">
                                <?php foreach ($medecins as $medecin): ?>
                                    <option value="<?= $medecin->getId() ?>"
                                        <?= $medecin->getId() == $patient->getMedecinId() ? 'selected' : '' ?>><?= $medecin ?></option>
                                <?php endforeach; ?>
                            </select>

                            <div class="br">
                                <!-- Champ date  -->
                                <label for="date">Date consultation : </label>
                                <input type="datetime-local" id="date" name="date">
                            </div>

                            <label for="duree">Durée (minutes)</label>
                            <input type="text" id="duree" name="duree" value="30" required>

                            <?php if ($consultationError): ?>
                                <span class="error" aria-live="polite">Erreur durant la création: Vérifiez vos informations !</span>
                            <?php elseif ($consultationSuccess): ?>
                                <span class="Validate" aria-live="polite">Création effectuée !</span>
                            <?php endif; ?>

                            <input type="submit" id="submit" name="submit" value="Créer">
                        </form>
                    </fieldset>
                </div>

                <?php if (hasSelectedConsultation($consultations)): ?>
                    <div class="minispace">
                        <!-- Troisième fieldset -->
                        <fieldset>
                            <legend><h3> Consultation sélectionnée</h3></legend>
                            <?php if ($modifyConsultation): ?>
                            <form method="POST">
                                <?php endif; ?>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th><span class="Medecin">Médecin</span></th>
                                        <th><span class="DateRDV">Date RDV</span></th>
                                        <th><span class="Heure">Heure</span></th>
                                        <th><span class="Heure">Durée</span></th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php foreach ($consultations as $consultation):
                                        if (($_GET["idCon"] ?? -1) != $consultation->getId()):
                                            continue;
                                        endif;
                                        ?>
                                        <tr>
                                            <?php if ($modifyConsultation && $consultation->getId() == $idCon): ?>
                                                <td>
                                                    <label>
                                                        <select name="medecin">
                                                            <?php foreach ($medecins as $medecin): ?>
                                                                <option value="<?= $medecin->getId() ?>"
                                                                    <?= $medecin->getId() == $consultation->getIdMedecin() ? 'selected' : '' ?>><?= $medecin ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </label>
                                                </td>
                                                <td><input type="date" id="date" name="date"
                                                           value="<?= date("Y-m-d", $consultation->getDateRdv()) ?>">
                                                </td>
                                                <td><label for="time"></label><input type="time" id="time" name="time"
                                                                                     value="<?= date("H:i", $consultation->getDateRdv()) ?>">
                                                </td>
                                                <td><input type="text" id="duree" name="duree" placeholder="minutes"
                                                           value="<?= $consultation->getDuree() ?>" required></td>
                                                <td>
                                                    <label>
                                                        <input name="IDAC" value="<?= $consultation->getId() ?>" hidden>
                                                    </label>
                                                    <input type="submit" id="applyCon" name="applyCon" value="Appliquer"
                                                           class="reduce">
                                                </td>
                                            <?php else: ?>
                                                <td><?= $consultation->getMedecin() ?? 'Erreur récupération médecin' ?></td>
                                                <td><?= date("d/m/Y", $consultation->getDateRdv()) ?></td>
                                                <td><?= date("H:i", $consultation->getDateRdv()) ?></td>
                                                <td><?= $consultation->getDuree() . ' minutes' ?></td>
                                                <td>
                                                    <form method="post">
                                                        <label>
                                                            <input name="IDC" value="<?= $consultation->getId() ?>"
                                                                   hidden>
                                                        </label>

                                                        <?php if ($consultation->getDateRdv() > time()): ?>
                                                            <input type="submit" id="modify" name="modify"
                                                                   value="Modifier"
                                                                   class="reduce">
                                                        <?php endif; ?>

                                                        <input type="submit" id="delete" name="delete" value="Supprimer"
                                                               class="reduce"
                                                               onclick="if (!confirm('Confirmer supression ?')) return false">
                                                    </form>
                                                </td>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <?php if ($modifyConsultation): ?>
                            </form>
                        <?php endif; ?>

                            <?php if ($modifyConsultationError): ?>
                                <span class="error"
                                      aria-live="polite">Erreur durant la modification: Vérifiez vos changements !</span>
                            <?php endif; ?>
                        </fieldset>
                    </div>
                <?php endif; ?>

                <div class="minispace">
                    <fieldset> <!-- 4eme fieldset -->
                        <legend><h3> Historique Consultation</h3></legend>
                        <?php if ($modifyConsultation): ?>
                        <form method="POST">
                            <?php endif; ?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th><span class="Medecin">Médecin</span></th>
                                    <th><span class="DateRDV">Date RDV</span></th>
                                    <th><span class="Heure">Heure</span></th>
                                    <th><span class="Heure">Durée</span></th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach ($consultations as $consultation): ?>
                                    <tr>
                                        <?php if ($modifyConsultation && $consultation->getId() == $idCon): ?>
                                            <td>
                                                <label>
                                                    <select name="medecin">
                                                        <?php foreach ($medecins as $medecin): ?>
                                                            <option value="<?= $medecin->getId() ?>"
                                                                <?= $medecin->getId() == $consultation->getIdMedecin() ? 'selected' : '' ?>><?= $medecin ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </label>
                                            </td>
                                            <td><input type="date" id="date" name="date"
                                                       value="<?= date("Y-m-d", $consultation->getDateRdv()) ?>"></td>
                                            <td><label for="time"></label><input type="time" id="time" name="time"
                                                                                 value="<?= date("H:i", $consultation->getDateRdv()) ?>">
                                            </td>
                                            <td><input type="text" id="duree" name="duree" placeholder="minutes"
                                                       value="<?= $consultation->getDuree() ?>" required></td>
                                            <td>
                                                <label>
                                                    <input name="IDAC" value="<?= $consultation->getId() ?>" hidden>
                                                </label>
                                                <input type="submit" id="applyCon" name="applyCon" value="Appliquer"
                                                       class="reduce">
                                            </td>
                                        <?php else: ?>
                                            <td><?= $consultation->getMedecin() ?? 'Erreur récupération médecin' ?></td>
                                            <td><?= date("d/m/Y", $consultation->getDateRdv()) ?></td>
                                            <td><?= date("H:i", $consultation->getDateRdv()) ?></td>
                                            <td><?= $consultation->getDuree() . ' minutes' ?></td>
                                            <td>
                                                <form method="post">
                                                    <label>
                                                        <input name="IDC" value="<?= $consultation->getId() ?>" hidden>
                                                    </label>

                                                    <?php if ($consultation->getDateRdv() > time()): ?>
                                                        <input type="submit" id="modify" name="modify" value="Modifier"
                                                               class="reduce">
                                                    <?php endif; ?>

                                                    <input type="submit" id="delete" name="delete" value="Supprimer"
                                                           class="reduce"
                                                           onclick="if (!confirm('Confirmer supression ?')) return false">
                                                </form>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php if ($modifyConsultation): ?>
                        </form>
                    <?php endif; ?>

                        <?php if ($modifyConsultationError): ?>
                            <span class="error"
                                  aria-live="polite">Erreur durant la modification: Vérifiez vos changements !</span>
                        <?php endif; ?>
                    </fieldset>
                </div>

                <form method="POST">
                    <input type="submit" id="deleteUS" name="deleteUS" value="Supprimer l'usager" class="reduce"
                           onclick="if (!confirm('Confirmer supression ?')) return false">
                </form>
            <?php else: ?>
                <p>Erreur interne: récupération du patient/usager !</p>
            <?php endif; ?>
        </div>
    </div>
</main>
<?php require './layout/footer.php'; ?>
</body>
</html>
<script type="text/javascript" src="./js/app.js"></script>