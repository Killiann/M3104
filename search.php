<?php

use M3104\patient\PatientManager;

require './layout/middleware.php';

$error = false;
if (!isset($_GET["lastName"])) { // Si le lien de la page est incorrect on affiche une erreur
    $error = true;
} else {
    // On recherche les patients en fonction de $_GET["lastName"]
    $patients = PatientManager::getInstance()->getAdapter()->search($_GET["lastName"]);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recherche</title>
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>

<body>
<?php require './layout/header.php'; ?>
<!-- Contenue de la page -->
<main>
    <div class="container">
        <div class="back content">
            <h1 class="bold">RECHERCHE</h1>

            <?php if ($error): ?>
                <p>Erreur argument de recherche.</p>
            <?php elseif (!empty($patients)):
                foreach ($patients as $patient): ?>
                    <form action="user" method="GET">
                        <p class="center"><?= $patient ?></p>
                        <label>
                            <input name="ID" value="<?= $patient->getId() ?>" hidden>
                        </label>
                        <input type='submit' value='Selectionner'>
                    </form>
                <?php endforeach;
            else: ?>
                <p>Aucun résultats...</p>
            <?php endif; ?>
        </div>
    </div>
</main>
<?php require './layout/footer.php'; ?>
</body>
</html>