<?php

use M3104\secretaire\SecretaireManager;
use M3104\util\Utils;

session_start();

// Si la personne qui veut accéder à cette page est connecté elle est redirigé vers la page principale
if (isset($_SESSION['user'])) {
    header('Location: ./index');
    return;
}

// Load automatiquement toutes les classes
require './class/Autoloader.php';
M3104\Autoloader::register();

// Permet d'éviter les injections
Utils::serialize();

$error = false;

if (isset($_POST['submit'])) { // Si l'utilisateur a cliqué sur le bouton connexion
    $user = find("username");
    $password = find("password");

    $id = SecretaireManager::getInstance()->login($user, $password);
    if ($id != 0) { // Si l'utilisateur et le mot de passe sont valides
        $_SESSION["user"] = $id;
        header("Location: ./index");
        return;
    } else {
        $error = true; // On affiche une erreur
    }
}

/**
 * Retourne la valeur de la clé $nom dans la variable globale $_POST ou un string vide
 *
 * @param $nom
 * @return string
 */
function find($nom): string
{
    return $_POST[$nom] ?? '';
}

?>
<html lang="fr">
<head>
    <title>Connexion</title>
    <meta charset="utf-8">
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>
<body>
<div id="container">
    <!-- formulaire de connexion -->
    <form method="POST">
        <h1>Connexion</h1>

        <label for="username"><b>Nom d'utilisateur</b></label>
        <input id="username" type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>

        <label for="password"><b>Mot de passe</b></label>
        <input id="password" type="password" placeholder="Entrer le mot de passe" name="password" required>

        <!-- Affichage de l'erreur si les identifiants sont invalides -->
        <?php if ($error): ?>
            <span class="center error" aria-live="polite">Identifiants invalides !</span>
        <?php endif; ?>

        <input type="submit" id='submit' value='CONNEXION' name='submit'>
    </form>
</div>
</body>
</html>