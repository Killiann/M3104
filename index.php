<?php

use M3104\medecin\MedecinManager;
use M3104\patient\PatientManager;

require './layout/middleware.php';

$success = false;
$error = false;
if (isset($_POST['submit'])) { // Si l'utilisateur clique sur le boutton pour créer un patient
    try {
        $patient = PatientManager::getInstance()->createPatient();
        $success = true;
    } catch (Exception $e) { // Si une exception est throw alors on affiche une erreur
        $error = true;
    }
}

// On récupère tous les médecins
$medecins = MedecinManager::getInstance()->getAdapter()->getAll();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Usager</title>
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>

<body>
<?php require './layout/header.php'; ?>

<!-- Contenue de la page -->
<main>
    <div class="container">
        <div class="back content">
            <h1 class="bold minispace">USAGERS</h1>

            <fieldset>
                <legend><h3> Recherche Usager </h3></legend>
                <form action="search" method="GET">
                    <label for="lastName">Nom</label>
                    <input type="text" id="lastName" name="lastName" required>
                    <input type="submit" value="Recherche">
                </form>
            </fieldset>

            <fieldset>
                <legend><h3> Liste des usagers </h3></legend>
                <form action="list" method="GET">
                    <label>
                        <input name="page" value="1" hidden>
                    </label>
                    <input type="submit" value="Afficher">
                </form>
            </fieldset>

            <fieldset>
                <legend><h3> Saisie d'un Usager </h3></legend>
                <form method="POST">
                    <!-- Select de la Civilité -->
                    <label>
                        <select name="civilite">
                            <option value="M">Monsieur</option>
                            <option value="F">Madame</option>
                        </select>
                    </label>

                    <!-- Champ Nom -->
                    <div class="br">
                        <label for="nom">Nom</label>
                    </div>
                    <input type="text" id="nom" name="nom" required>

                    <!-- Champ Prenom -->
                    <label for="prenom">Prenom</label>
                    <input type="text" id="prenom" name="prenom" required>

                    <div class="br">
                        <!-- Champ date Naissance -->
                        <label for="dateNassaince"> Date de naissance : </label>
                        <input type="date" id="dateNassaince" name="dateNassaince">
                    </div>

                    <!-- Champ Adresse -->
                    <label for="adresse">Adresse</label>
                    <input type="text" id="adresse" name="adresse" required>

                    <!-- Champ lieu naissance -->
                    <label for="lieuNaissance">Lieu de naissance</label>
                    <input type="text" id="lieuNaissance" name="lieuNaissance" required>

                    <!-- Champ NumSecu -->
                    <label for="NumSecu">Numéro de sécurité social (15 chiffres)</label>
                    <input type="text" id="NumSecu" name="NumSecu" required>

                    <!-- Champ medecin traitant -->
                    <div class="br">
                        <label for="medecin">Médecin traitant</label>
                        <select id="medecin" name="medecin">
                            <option value="-1">Aucun</option>
                            <?php foreach ($medecins as $medecin): ?>
                                <option value="<?= $medecin->getId() ?>"><?= $medecin ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <!-- Affichage de l'erreur ou de la confirmation -->
                    <?php if ($error): ?>
                        <span class="error" aria-live="polite">Erreur durant la création !</span>
                    <?php elseif ($success): ?>
                        <span class="Validate" aria-live="polite">Création effectuée !</span>
                    <?php endif; ?>

                    <input type="submit" id="submit" name="submit" class="creer" value="Créer">
                </form>
            </fieldset>
        </div>
    </div>
</main>
<?php include './layout/footer.php' ?>
</body>
</html>
<script type="text/javascript" src="./js/app.js"></script>