<?php

namespace M3104\util;

class Utils
{
    /**
     * Enlève les injections dans les variables globales $_POST et $_GET
     */
    public static function serialize()
    {
        foreach ($_POST as $key => $value) {
            $_POST[$key] = htmlentities($value);
        }
        foreach ($_GET as $key => $value) {
            $_GET[$key] = htmlentities($value);
        }
    }

    /**
     * Retourne true si une liste de clés sont présentes dans la variable globale $_POST
     *
     * @param array $vals
     * @return bool
     */
    public static function isValid(array $vals): bool
    {
        foreach ($vals as $val) {
            if (!isset($_POST[$val])) return false;
        }
        return true;
    }
}