<?php

namespace M3104\sql;

/**
 * Classe abstraite utitlisé pour créer une classe pour gérer un objet Adaptable
 *
 * Amélioration possible: Utiliser un type généric avec PHP 8.0
 */
abstract class Adapter
{

    abstract public function update(Adaptable $adaptable);

    abstract public function insert(Adaptable $adaptable);

    abstract public function delete(int $id);

    abstract public function getAll(): array;

}