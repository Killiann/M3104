<?php

namespace M3104\sql;

use PDO;

class SQL
{

    private static $instance = null;

    private $con;

    private function __construct(bool $fixLimit)
    {
        $this->con = new PDO('mysql:dbname=projetweb;host=localhost', getenv('USERSQLMED'), getenv('PASSWSQLMED'));
        if ($fixLimit) {
            $this->con->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE); // Pour que le LIMIT marche avec PDO
        }
    }

    /**
     * Retourne la connexion SQL
     *
     * @return PDO
     */
    public function getCon(): PDO
    {
        return $this->con;
    }

    /**
     * Retourne l'instance du sigleton SQL en appliquant ou non un fix pour le LIMIT
     *
     * @param bool $fixLimit
     * @return SQL
     */
    public static function getInstance(bool $fixLimit = false): SQL
    {
        if (is_null(SQL::$instance)) {
            SQL::$instance = new SQL($fixLimit);
        }
        return SQL::$instance;
    }
}