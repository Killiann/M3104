<?php

namespace M3104\medecin;

use Exception;
use M3104\sql\Adaptable;
use M3104\sql\Adapter;
use M3104\sql\SQL;

class MedecinAdapter extends Adapter
{

    /**
     * Met à jour un médecin dans la base de données
     *
     * @param Adaptable $adaptable
     * @throws Exception si l'instance mise à jour n'est pas un médecin
     */
    public function update(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Medecin)) {
            throw new Exception('update: type Adaptable incorrect');
        }

        $req = SQL::getInstance()->getCon()->prepare('UPDATE medecin SET civilite = ?, prenom = ?, nom = ? WHERE id_medecin = ?');
        $req->execute(array($adaptable->getCivility(), $adaptable->getName(), $adaptable->getLastName(), $adaptable->getId()));
    }

    /**
     * Ajoute un médecin à la base de données
     *
     * @param Adaptable $adaptable
     * @throws Exception si l'instance mise à jour n'est pas un médecin
     */
    public function insert(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Medecin)) {
            throw new Exception('insert: type Adaptable incorrect');
        }

        $con = SQL::getInstance()->getCon();
        $req = $con->prepare('INSERT INTO medecin(civilite, prenom, nom) VALUES(?, ?, ?)');
        $req->execute(array($adaptable->getCivility(), $adaptable->getName(), $adaptable->getLastName()));
        $adaptable->setId($con->lastInsertId());
    }

    /**
     * Supprime un médecin dans la base de données en fonction de son id
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        $req = SQL::getInstance()->getCon()->prepare('DELETE FROM medecin WHERE id_medecin = ?');
        $req->execute(array($id));
    }

    /**
     * Retourne la liste de tous les médecins
     *
     * @return array
     */
    public function getAll(): array
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM medecin');
        $req->execute();

        $medecins = array();
        foreach ($req->fetchAll() as $result) {
            $medecin = $this->reqToMedecin($result);
            if (!is_null($medecin)) {
                array_push($medecins, $medecin);
            }
        }
        return $medecins;
    }

    /**
     * Retourne un médecin en fonction de son id si elle existe
     *
     * @param int $id
     * @return Medecin|null
     */
    public function get(int $id): ?Medecin
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM medecin WHERE id_medecin = ?');
        $req->execute(array($id));
        return $this->reqToMedecin($req->fetch());
    }

    /**
     * Retourne un médecin construit à partir du résultat d'une requête SQL si celle-ci est valide
     *
     * @param $req
     * @return Medecin|null
     */
    private function reqToMedecin($req): ?Medecin
    {
        return !is_null($req['id_medecin']) ?
            new Medecin($req['id_medecin'], $req['civilite'], $req['nom'], $req['prenom']) : null;
    }
}