<?php

namespace M3104\medecin;

use Exception;
use M3104\util\Utils;

class MedecinManager
{

    private static $instance = null;
    private $adapter;

    private function __construct()
    {
        $this->adapter = new MedecinAdapter();
    }

    /**
     * Retourne l'adapter SQL pour gérer les médecins
     *
     * @return MedecinAdapter
     */
    public function getAdapter(): MedecinAdapter
    {
        return $this->adapter;
    }

    /**
     * Retourne un médecin si son id est contenu dans une liste de médecins
     *
     * @param array $medecins
     * @param int $medecinId
     * @return Medecin|null
     */
    public function getMedecin(array $medecins, int $medecinId): ?Medecin
    {
        foreach ($medecins as $medecin) {
            if ($medecin->getId() == $medecinId) {
                return $medecin;
            }
        }
        return Null;
    }

    /**
     * Crée un médecin en fonction des informations stockées dans la variable globale $_POST
     *
     * @return Medecin
     * @throws Exception si le formulaire(varaible globale $_POST) n'est pas valide
     */
    public function createMedecin(): Medecin
    {
        if ($this->hasError()) {
            throw new Exception('Formulaire medecin invalid');
        }

        $medecin = new Medecin(-1, $_POST['civilite'], $_POST['nom'], $_POST['prenom']);

        $this->adapter->insert($medecin);
        return $medecin;
    }

    /**
     * Met à jour un médecin en fonction de son id et des informations stockées dans la variable globale $_POST
     *
     * @param int $id
     * @throws Exception si le formulaire(varaible globale $_POST) n'est pas valide
     */
    public function updateMedecin(int $id)
    {
        if ($this->hasError()) {
            throw new Exception('Formulaire medecin invalid');
        }

        $this->adapter->update(new Medecin($id, $_POST['civilite'], $_POST['nom'], $_POST['prenom']));
    }

    /**
     * Retourne true si le formulaire(varaible globale $_POST) est valide
     *
     * @return bool
     */
    private function hasError(): bool
    {
        return (!isset($_POST['civilite']) || ($_POST['civilite'] != 'M' && $_POST['civilite'] != 'F'))
            || !Utils::isValid(array('nom', 'prenom'));
    }

    /**
     * Retourne l'instance du sigleton MedecinManager
     *
     * @return MedecinManager
     */
    public static function getInstance(): MedecinManager
    {
        if (is_null(MedecinManager::$instance)) {
            MedecinManager::$instance = new MedecinManager();
        }
        return MedecinManager::$instance;
    }
}