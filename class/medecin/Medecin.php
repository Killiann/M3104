<?php

namespace M3104\medecin;

use M3104\sql\Adaptable;

class Medecin implements Adaptable
{

    private $id;
    private $civility;
    private $lastName;
    private $name;

    public function __construct(int $id, string $civility, string $lastName, string $name)
    {
        $this->id = $id;
        $this->civility = $civility;
        $this->lastName = $lastName;
        $this->name = $name;
    }

    /**
     * Retourne l'id du médecin
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Permet de définir l'id d'un médecin
     *
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Retourne la civilité d'un médecin
     *
     * @return string (M ou F)
     */
    public function getCivility(): string
    {
        return $this->civility;
    }

    /**
     * Retourne la civilité formaté d'un médecin
     *
     * @return string (Monsieur ou Madame)
     */
    public function getCivilityFormatted(): string
    {
        return ($this->civility == "M" ? "Monsieur" : "Madame");
    }

    /**
     * Retourne le nom de famille d'un médecin
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Retourne le prénom d'un médecin
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->lastName . " " . $this->name . " (" . $this->getCivilityFormatted() . ")";
    }
}