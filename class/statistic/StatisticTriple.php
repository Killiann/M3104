<?php

namespace M3104\statistic;

class StatisticTriple extends StatisticDouble
{

    private $third;

    public function __construct($male, $female, $third)
    {
        parent::__construct($male, $female);
        $this->third = $third;
    }

    /**
     * Retourne la troisième valeur statistique
     *
     * @return mixed
     */
    public function getThird()
    {
        return $this->third;
    }
}