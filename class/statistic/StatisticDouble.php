<?php

namespace M3104\statistic;

class StatisticDouble
{

    private $first;
    private $second;

    public function __construct($male, $female)
    {
        $this->first = $male;
        $this->second = $female;
    }

    /**
     * Retourne la première valeur statistique
     *
     * @return mixed
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * Retourne la deuxieme valeur statistique
     *
     * @return mixed
     */
    public function getSecond()
    {
        return $this->second;
    }
}