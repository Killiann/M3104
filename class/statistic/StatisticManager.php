<?php

namespace M3104\statistic;

use M3104\sql\SQL;

class StatisticManager
{

    private static $instance = null;

    private function __construct()
    {
    }

    /**
     * Retourne le nombre de femme et d'homme patient avec une age dans l'intervalle [$year1, $year2]
     *
     * @param int $year1
     * @param int $year2
     * @return StatisticDouble|null
     */
    public function getStatisticCivility(int $year1, int $year2): ?StatisticDouble
    {
        $req = SQL::getInstance()->getCon()->prepare("SELECT 
       (SELECT COUNT(*) FROM patient p2 WHERE p2.civilite = 'M' AND 
                    (@TEMPSDIF := TIMESTAMPDIFF(YEAR, CAST(FROM_UNIXTIME(p2.date_naissance) as date), NOW())) >= :year1 AND @TEMPSDIF <= :year2) MAL, 
       (SELECT COUNT(*) FROM patient p2 WHERE p2.civilite = 'F' AND 
                    (@TEMPSDIF := TIMESTAMPDIFF(YEAR, CAST(FROM_UNIXTIME(p2.date_naissance) as date), NOW())) >= :year1 AND @TEMPSDIF <= :year2) FEM");
        $req->execute(array(':year1' => $year1, ":year2" => $year2));
        $result = $req->fetch();

        return !is_null($result['MAL']) && !is_null($result['FEM']) ? new StatisticDouble($result['MAL'], $result['FEM']) : Null;
    }

    /**
     * Retourne une liste de statisque sur les consultations en fonction des médecins
     * => Nombre d'heures de consultations
     * => Nombre de consultations
     * => Durée moyenne en minutes d'une consultation
     *
     * @return array
     */
    public function getStatisticConsultation(): array
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT 
            m.id_medecin,
            (SELECT COUNT(*) FROM consultation c WHERE c.id_medecin = m.id_medecin) NBC,
            (SELECT (SUM(c.duree)/60) FROM consultation c WHERE c.id_medecin = m.id_medecin) HOUR,
            (SELECT AVG(c.duree) FROM consultation c WHERE c.id_medecin = m.id_medecin) AVG
            FROM medecin m
            ORDER BY HOUR DESC');
        $req->execute();

        $stats = array();
        foreach ($req->fetchAll() as $result) {
            $stat = $this->getStatisticTripleConsultation($result);
            if (!is_null($stat)) {
                $stats[$result['id_medecin']] = $stat;
            }
        }

        return $stats;
    }

    /**
     * Retourne un StatisticTriple construit à partir du résultat d'une requête SQL si celle-ci est valide
     *
     * @param $result
     * @return StatisticTriple|null
     */
    private function getStatisticTripleConsultation($result): ?StatisticTriple
    {
        return !is_null($result['id_medecin']) ?
            new StatisticTriple($result['HOUR'] ?? 0, $result['NBC'], $result['AVG'] ?? 0) : Null;
    }

    /**
     * Retourne l'instance du sigleton MedecinManager
     *
     * @return StatisticManager
     */
    public static function getInstance(): StatisticManager
    {
        if (is_null(StatisticManager::$instance)) {
            StatisticManager::$instance = new StatisticManager();
        }
        return StatisticManager::$instance;
    }
}