<?php

namespace M3104\secretaire;

class SecretaireManager
{

    private static $instance = null;
    private $adapter;

    private function __construct()
    {
        $this->adapter = new SecretaireAdapter();
    }

    /**
     * Retourne l'adapter SQL pour gérer les secretaires
     *
     * @return SecretaireAdapter
     */
    public function getAdapter(): SecretaireAdapter
    {
        return $this->adapter;
    }

    /**
     * Retourne l'id d'une secretaire si son utilisateur et son mot de passe est valide sinon -1
     *
     * @param string $user
     * @param string $password
     * @return int
     */
    public function login(string $user, string $password): int
    {
        $secretaire = $this->adapter->getForLogin($user, $password);
        return !is_null($secretaire) ? $secretaire->getId() : 0;
    }

    /**
     * Retourne l'instance du sigleton SecretaireManager
     *
     * @return SecretaireManager
     */
    public static function getInstance(): SecretaireManager
    {
        if (is_null(SecretaireManager::$instance)) {
            SecretaireManager::$instance = new SecretaireManager();
        }
        return SecretaireManager::$instance;
    }
}