<?php

namespace M3104\secretaire;

use Exception;
use M3104\sql\Adaptable;
use M3104\sql\Adapter;
use M3104\sql\SQL;

class SecretaireAdapter extends Adapter
{

    /**
     * Met à jour une secretaire dans la base de données
     *
     * @param Adaptable $adaptable
     * @throws Exception si l'instance mise à jour n'est pas une secretaire
     */
    public function update(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Secretaire)) {
            throw new Exception('update: type Adaptable incorrect');
        }

        $req = SQL::getInstance()->getCon()->prepare('UPDATE secretaire SET utilisateur = ?, 
                      motdepasse = MD5(?) WHERE id_secretaire = ?');
        $req->execute(array($adaptable->getUser(), $adaptable->getPassword()));
    }

    /**
     * Ajoute une secretaire à la base de données
     *
     * @param Adaptable $adaptable
     * @throws Exception si l'instance mise à jour n'est pas une secretaire
     */
    public function insert(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Secretaire)) {
            throw new Exception('insert: type Adaptable incorrect');
        }

        $con = SQL::getInstance()->getCon();
        $req = $con->prepare('INSERT INTO secretaire(utilisateur, motdepasse) VALUES(?, MD5(?))');
        $req->execute(array($adaptable->getUser(), $adaptable->getPassword()));
        $adaptable->setId($con->lastInsertId());
    }

    /**
     * Supprime une secretaire dans la base de données en fonction de son id
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        $req = SQL::getInstance()->getCon()->prepare('DELETE FROM secretaire WHERE id_secretaire = ?');
        $req->execute(array($id));
    }

    /**
     * Retourne la liste de toutes les secretaires
     *
     * @return array
     */
    public function getAll(): array
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM secretaire');
        $req->execute();

        $secretaires = array();
        foreach ($req->fetchAll() as $result) {
            $secretaire = $this->reqToSecretaire($result);
            if (!is_null($secretaire)) {
                array_push($secretaires, $secretaire);
            }
        }
        return $secretaires;
    }

    /**
     * Retourne une secretaire en fonction de son id si elle existe
     *
     * @param int $id
     * @return Secretaire|null
     */
    public function get(int $id): ?Secretaire
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM secretaire WHERE id_secretaire = ?');
        $req->execute(array($id));
        return $this->reqToSecretaire($req->fetch());
    }

    /**
     * Retourne une secretaire en fonction de son utilisateur et de son mot de passe si elle existe
     *
     * @param string $user
     * @param string $password
     * @return Secretaire|null
     */
    public function getForLogin(string $user, string $password): ?Secretaire
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM secretaire WHERE utilisateur = ? 
                           AND motdepasse = MD5(?)');
        $req->execute(array($user, $password));
        return $this->reqToSecretaire($req->fetch());
    }

    /**
     * Retourne une secretaire construit à partir du résultat d'une requête SQL si celle-ci est valide
     *
     * @param $req
     * @return Secretaire|null
     */
    private function reqToSecretaire($req): ?Secretaire
    {
        return !is_null($req['id_secretaire']) ?
            new Secretaire($req['id_secretaire'], $req['utilisateur'], $req['motdepasse']) : null;
    }
}