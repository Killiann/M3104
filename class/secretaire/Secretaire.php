<?php

namespace M3104\secretaire;

use M3104\sql\Adaptable;

class Secretaire implements Adaptable
{

    private $id;
    private $user;
    private $password;

    public function __construct(int $id, string $user, string $password)
    {
        $this->id = $id;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Retourne l'id d'une secretaire
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Permet de définir l'id d'une secretaire
     *
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Retourne l'utilisateur de connection d'une secretaire
     *
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * Retourne le mot de passe de connection(encodé en MD5) d'une secretaire
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function __toString()
    {
        return $this->user;
    }
}