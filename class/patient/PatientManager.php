<?php

namespace M3104\patient;

use DateTime;
use Exception;
use M3104\util\Utils;

class PatientManager
{

    private static $instance = null;
    private $adapter;

    private function __construct()
    {
        $this->adapter = new PatientAdapter();
    }

    /**
     * Retourne l'adapter SQL pour gérer les patients
     *
     * @return PatientAdapter
     */
    public function getAdapter(): PatientAdapter
    {
        return $this->adapter;
    }

    /**
     * Crée un patient en fonction des informations stockées dans la variable globale $_POST
     *
     * @return Patient
     * @throws Exception si le formulaire(varaible globale $_POST) n'est pas valide
     */
    public function createPatient(): Patient
    {
        if ($this->hasError()) {
            throw new Exception('Formulaire usager invalid');
        }
        $patient = new Patient(-1, $_POST['civilite'], $_POST['nom'], $_POST['prenom'], $_POST['adresse'],
            DateTime::createFromFormat("Y-m-d", $_POST['dateNassaince'])->getTimestamp(),
            $_POST['lieuNaissance'], $_POST['NumSecu'], $_POST['medecin']);

        $this->adapter->insert($patient);
        return $patient;
    }

    /**
     * Met à jour un patient en fonction de son id et des informations stockées dans la variable globale $_POST
     *
     * @param int $id
     * @return Patient
     * @throws Exception si le formulaire(varaible globale $_POST) n'est pas valide
     */
    public function updatePatient(int $id): Patient
    {
        if ($this->hasError()) {
            throw new Exception('Formulaire usager invalid');
        }
        $patient = new Patient($id, $_POST['civilite'], $_POST['nom'], $_POST['prenom'], $_POST['adresse'],
            DateTime::createFromFormat("Y-m-d", $_POST['dateNassaince'])->getTimestamp(),
            $_POST['lieuNaissance'], $_POST['NumSecu'], $_POST['medecin']);

        $this->adapter->update($patient);
        return $patient;
    }

    /**
     * Retourne true si le formulaire(varaible globale $_POST) est valide
     *
     * @return bool
     */
    private function hasError(): bool
    {
        $d = DateTime::createFromFormat("Y-m-d", $_POST['dateNassaince']);
        return !$d || (!isset($_POST['civilite']) || ($_POST['civilite'] != 'M' && $_POST['civilite'] != 'F'))
            || !Utils::isValid(array('nom', 'prenom', 'dateNassaince', 'adresse', 'lieuNaissance'))
            || (!isset($_POST['NumSecu']) || !is_numeric($_POST['NumSecu']) || strlen($_POST['NumSecu']) != 15)
            || (!isset($_POST['medecin']) || !is_numeric($_POST['medecin']));
    }

    /**
     * Retourne l'instance du sigleton PatientManager
     *
     * @return PatientManager
     */
    public static function getInstance(): PatientManager
    {
        if (is_null(PatientManager::$instance)) {
            PatientManager::$instance = new PatientManager();
        }
        return PatientManager::$instance;
    }
}