<?php

namespace M3104\patient;

use Exception;
use M3104\sql\Adaptable;
use M3104\sql\Adapter;
use M3104\sql\SQL;

class PatientAdapter extends Adapter
{

    /**
     * Met à jour un patient dans la base de données
     *
     * @param Adaptable $adaptable
     * @throws Exception si l'instance mise à jour n'est pas un patient
     */
    public function update(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Patient)) {
            throw new Exception('update: type Adaptable incorrect');
        }

        $req = SQL::getInstance()->getCon()->prepare('UPDATE patient SET 
                   civilite = ?, nom = ?, prenom = ?, adresse = ?, date_naissance = ?, lieu_naissance = ?, 
                   secu_sociale = ?, id_medecin = ? WHERE id_patient = ?');
        $req->execute(array($adaptable->getCivility(), $adaptable->getLastName(), $adaptable->getName(), $adaptable->getLocation(),
            $adaptable->getBirthDate(), $adaptable->getBirthLocation(),
            $adaptable->getSecurityNumber(), $adaptable->getMedecinId(), $adaptable->getId()));
    }

    /**
     * Ajoute un patient à la base de données
     *
     * @param Adaptable $adaptable
     * @throws Exception si l'instance mise à jour n'est pas un patient
     */
    public function insert(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Patient)) {
            throw new Exception('insert: type Adaptable incorrect');
        }

        $con = SQL::getInstance()->getCon();
        $req = $con->prepare('INSERT INTO patient(civilite, nom, prenom, adresse, date_naissance, 
                    lieu_naissance, secu_sociale, id_medecin) VALUES(?, ?, ?, ?, ?, ?, ?, ?)');
        $req->execute(array($adaptable->getCivility(), $adaptable->getLastName(), $adaptable->getName(),
            $adaptable->getLocation(), $adaptable->getBirthDate(), $adaptable->getBirthLocation(),
            $adaptable->getSecurityNumber(), $adaptable->getMedecinId() == -1 ? Null : $adaptable->getMedecinId()));
        $adaptable->setId($con->lastInsertId());
    }

    /**
     * Supprime un patient dans la base de données en fonction de son id
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        $req = SQL::getInstance()->getCon()->prepare('DELETE FROM patient WHERE id_patient = ?');
        $req->execute(array($id));
    }

    /**
     * Retourne la liste de tous les patients
     *
     * @return array
     */
    public function getAll(): array
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM patient');
        $req->execute();
        return $this->createArray($req);
    }

    /**
     * Retourne la liste de tous les patients dans l'intervalle [(page-1)*10, ((page-1)*10)+10]
     * Utilisé pour faire un système de pagination
     *
     * @param int $page
     * @return array
     */
    public function getPage(int $page): array
    {
        $req = SQL::getInstance(true)->getCon()->prepare('SELECT * FROM patient LIMIT ?, 10');
        $req->execute(array(($page - 1) * 10));
        return $this->createArray($req);
    }

    /**
     * Retourne un patient en fonction de son id si elle existe
     *
     * @param int $id
     * @return Patient|null
     */
    public function get(int $id): ?Patient
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM patient WHERE id_patient = ?');
        $req->execute(array($id));
        return $this->reqToPatient($req->fetch());
    }

    /**
     * Retourne la liste des patients trouvé en fonction du $lastName recherché
     *
     * @param string $lastName
     * @return array
     */
    public function search(string $lastName): array
    {
        $req = SQL::getInstance()->getCon()->prepare("SELECT * FROM patient WHERE 
                            (@SLUG := (REPLACE(LOWER(nom), ' ', '-')) LIKE CONCAT(LOWER(?), '%')) OR 
                            (@SLUG SOUNDS LIKE CONCAT(LOWER(?), '%'))");
        $req->execute(array($lastName, $lastName));
        return $this->createArray($req);
    }

    /**
     * Retourne un patient construit à partir du résultat d'une requête SQL si celle-ci est valide
     *
     * @param $req
     * @return Patient|null
     */
    private function reqToPatient($req): ?Patient
    {
        return !is_null($req['id_patient']) ?
            new Patient($req['id_patient'], $req['civilite'], $req['nom'], $req['prenom'], $req['adresse'],
                $req['date_naissance'], $req['lieu_naissance'], $req['secu_sociale'],
                !is_null($req['id_medecin']) ? $req['id_medecin'] : -1) : null;
    }

    /**
     * Créer une liste de patients en fonction d'une requête SQL
     *
     * @param $req
     * @return array
     */
    private function createArray($req): array
    {
        $patients = array();
        foreach ($req->fetchAll() as $result) {
            $patient = $this->reqToPatient($result);
            if (!is_null($patient)) {
                array_push($patients, $patient);
            }
        }
        return $patients;
    }
}