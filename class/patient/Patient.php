<?php

namespace M3104\patient;

use M3104\sql\Adaptable;

class Patient implements Adaptable
{

    private $id;
    private $civility;
    private $lastName;
    private $name;
    private $location;
    private $birthDate;
    private $birthLocation;
    private $securityNumber;
    private $medecinId;

    public function __construct(int $id, string $civility, string $lastName, string $name, string $location,
                                int $birthDate, string $birthLocation, string $securityNumber, int $medecinId)
    {
        $this->id = $id;
        $this->civility = $civility;
        $this->lastName = $lastName;
        $this->name = $name;
        $this->location = $location;
        $this->birthDate = $birthDate;
        $this->birthLocation = $birthLocation;
        $this->securityNumber = $securityNumber;
        $this->medecinId = $medecinId;
    }

    /**
     * Retourne l'id d'un patient
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Retourne la civilité d'un patient
     *
     * @return string (M ou F)
     */
    public function getCivility(): string
    {
        return $this->civility;
    }

    /**
     * Retourne la civilité formaté d'un médecin
     *
     * @return string (Monsieur ou Madame)
     */
    public function getCivilityFormatted(): string
    {
        return ($this->civility == "M" ? "Monsieur" : "Madame");
    }

    /**
     * Retourne le nom de famille d'un patient
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Retourne le prénom d'un patient
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Retourne l'adresse d'un patient
     *
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * Retourne le timestamp de la date de naissance d'un médecin
     *
     * @return int
     */
    public function getBirthDate(): int
    {
        return $this->birthDate;
    }

    /**
     * Retourne l'adresse de la naissance d'un patient
     *
     * @return string
     */
    public function getBirthLocation(): string
    {
        return $this->birthLocation;
    }

    /**
     * Retourne le numéro de sécurité social d'un patient
     *
     * @return string
     */
    public function getSecurityNumber(): string
    {
        return $this->securityNumber;
    }

    /**
     * Retourne l'id du médecin traitent d'un patient
     *
     * @return int
     */
    public function getMedecinId(): int
    {
        return $this->medecinId;
    }

    /**
     * Permet de définir l'id d'un patient
     *
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function __toString()
    {
        return $this->getCivilityFormatted() . " " . $this->lastName . " " . $this->name;
    }
}