<?php

namespace M3104\consultation;

use Exception;
use M3104\sql\Adaptable;
use M3104\sql\Adapter;
use M3104\sql\SQL;

class ConsultationAdapter extends Adapter
{

    /**
     * Met à jour une consultation dans la base de données si la nouvelle plage horraire est valide
     *
     * @param Adaptable $adaptable
     * @throws Exception si la plage horraire n'est pas valie
     */
    public function update(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Consultation) || !$this->canInsert($adaptable)) {
            throw new Exception('update: type Adaptable incorrect: ');
        }

        $req = SQL::getInstance()->getCon()->prepare('UPDATE consultation SET date_rdv = ?, duree = ?, id_medecin = ? WHERE id_consultation = ?');
        $req->execute(array($adaptable->getDateRdv(), $adaptable->getDuree(), $adaptable->getIdMedecin(), $adaptable->getId()));
    }

    /**
     * Ajoute une consultation dans la base de données si la plage horraire est valide
     *
     * @param Adaptable $adaptable
     * @throws Exception si la plage horraire n'est pas valie
     */
    public function insert(Adaptable $adaptable)
    {
        if (!($adaptable instanceof Consultation) || !$this->canInsert($adaptable)) {
            throw new Exception('Erreur insertion Consultation');
        }

        $con = SQL::getInstance()->getCon();
        $req = $con->prepare('INSERT INTO consultation(date_rdv, duree, id_medecin, id_patient, id_secretaire) VALUES(?, ?, ?, ?, ?)');
        $req->execute(array($adaptable->getDateRdv(), $adaptable->getDuree(), $adaptable->getIdMedecin(), $adaptable->getIdPatient(), $adaptable->getIdSecretaire()));
        $adaptable->setId($con->lastInsertId());
    }

    /**
     * Vérifie si la plage horraire d'une consultation est valide
     *
     * @param Adaptable $adaptable
     * @return bool true si la plage horraire est valide
     * @throws Exception si l'instance vérifié n'est pas une consultation
     */
    public function canInsert(Adaptable $adaptable): bool
    {
        if (!($adaptable instanceof Consultation)) {
            throw new Exception('canInsert: type Adaptable incorrect');
        }
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM `consultation` WHERE 
                                   (id_medecin = :medecin) AND (id_consultation != :id) AND
                                   ((:start <= date_rdv) OR (:start <= (date_rdv+(duree*60)))) AND 
                                   ((:end >= date_rdv) OR (:end >= (date_rdv+(duree*60))))');
        $req->execute(array(":id" => $adaptable->getId(), ":medecin" => $adaptable->getIdMedecin(), ":start" => $adaptable->getDateRdv(), ":end" => $adaptable->getEndRdv()));

        return $req->rowCount() == 0;
    }

    /**
     * Supprime une consultation dans la base de données en fonction de son id
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        $req = SQL::getInstance()->getCon()->prepare('DELETE FROM consultation WHERE id_consultation = ?');
        $req->execute(array($id));
    }

    /**
     * Retourne la liste de toutes les consultations
     *
     * @return array
     */
    public function getAll(): array
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM consultation');
        $req->execute();
        return $this->createArray($req);
    }

    /**
     * Return la liste des consultation d'un medecin si medecinId != -1 sinon toutes les consultations à une date
     *
     * @param int $medecinId
     * @param string $date
     * @return array
     */
    public function getAllOrMedecin(int $medecinId, string $date): array
    {
        $req = SQL::getInstance()->getCon()->prepare("SELECT * FROM consultation WHERE 
                                   IF(:medId = -1, true, id_medecin = :medId) AND 
                                 DATE_FORMAT(CAST(FROM_UNIXTIME(date_rdv) as date), '%Y-%m-%d') = :date 
                                ORDER BY date_rdv");
        $req->execute(array(":medId" => $medecinId, ":date" => $date));

        $consultations = array();
        foreach ($req->fetchAll() as $result) {
            $consultation = $this->reqToConsultation($result);
            if (!is_null($consultation)) {
                if (!array_key_exists($consultation->getIdMedecin(), $consultations)) {
                    $consultations[$consultation->getIdMedecin()] = array();
                }
                array_push($consultations[$consultation->getIdMedecin()], $consultation);
            }
        }
        return $consultations;
    }

    /**
     * Retourne la liste des consultations d'un patient/usager
     *
     * @param int $id
     * @return array
     */
    public function getFromPatient(int $id): array
    {
        $req = SQL::getInstance()->getCon()->prepare('SELECT * FROM consultation WHERE id_patient = ? ORDER BY date_rdv');
        $req->execute(array($id));
        return $this->createArray($req);
    }

    /**
     * Retourne une consultation construit à partir du résultat d'une requête SQL si celle-ci est valide
     *
     * @param $req
     * @return Consultation|null
     */
    private function reqToConsultation($req): ?Consultation
    {
        return !is_null($req['id_consultation']) ?
            new Consultation($req['id_consultation'], $req['date_rdv'], $req['duree'], $req['id_patient'], $req['id_medecin'], $req['id_secretaire']) : null;
    }

    /**
     * Créer une liste de consultations en fonction d'une requête SQL
     *
     * @param $req
     * @return array
     */
    private function createArray($req): array
    {
        $consultations = array();
        foreach ($req->fetchAll() as $result) {
            $consultation = $this->reqToConsultation($result);
            if (!is_null($consultation)) {
                array_push($consultations, $consultation);
            }
        }
        return $consultations;
    }
}