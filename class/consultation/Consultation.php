<?php

namespace M3104\consultation;

use M3104\medecin\Medecin;
use M3104\medecin\MedecinManager;
use M3104\patient\Patient;
use M3104\patient\PatientManager;
use M3104\secretaire\Secretaire;
use M3104\secretaire\SecretaireManager;
use M3104\sql\Adaptable;

class Consultation implements Adaptable
{

    private $id;
    private $dateRdv;
    private $duree;
    private $idPatient;
    private $idMedecin;
    private $idSecretaire;

    public function __construct(int $id, int $dateRdv, int $duree, int $idPatient, int $idMedecin, int $idSecretaire)
    {
        $this->id = $id;
        $this->dateRdv = $dateRdv;
        $this->duree = $duree;
        $this->idPatient = $idPatient;
        $this->idMedecin = $idMedecin;
        $this->idSecretaire = $idSecretaire;
    }

    /**
     * Retourne l'id de la consultation
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Retourne le timestamp de la date de la consultation
     *
     * @return int
     */
    public function getDateRdv(): int
    {
        return $this->dateRdv;
    }

    /**
     * Retourne le timestamp de la date de la fin de la consultation
     *
     * @return int
     */
    public function getEndRdv(): int
    {
        return $this->dateRdv + ($this->duree * 60);
    }

    /**
     * Retourne la durée en minute de la consultation
     *
     * @return int
     */
    public function getDuree(): int
    {
        return $this->duree;
    }

    /**
     * Retourne l'id du medecin associé à la consultation
     *
     * @return int
     */
    public function getIdMedecin(): int
    {
        return $this->idMedecin;
    }

    /**
     * Retourne une instance de Medecin si l'id de celui-ci existe
     *
     * @return Medecin|null
     */
    public function getMedecin(): ?Medecin
    {
        return MedecinManager::getInstance()->getAdapter()->get($this->idMedecin);
    }

    /**
     * Retourne l'id du patient/usager associé à la consultation
     *
     * @return int
     */
    public function getIdPatient(): int
    {
        return $this->idPatient;
    }

    /**
     * Returne une instance de Patient si l'id de celui-ci existe
     *
     * @return Patient|null
     */
    public function getPatient(): ?Patient
    {
        return PatientManager::getInstance()->getAdapter()->get($this->idPatient);
    }

    /**
     * Retourne l'id de la secretaire associé à la consultation
     *
     * @return int
     */
    public function getIdSecretaire(): int
    {
        return $this->idSecretaire;
    }

    /**
     * Returne une instance de Secretaire si l'id de celle-ci existe
     *
     * @return Secretaire|null
     */
    public function getSecretaire(): ?Secretaire
    {
        return SecretaireManager::getInstance()->getAdapter()->get($this->idSecretaire);
    }

    /**
     * Permet de définir l'id de la consultation
     *
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}