<?php

namespace M3104\consultation;

use DateTime;
use Exception;
use M3104\util\Utils;

class ConsultationManager
{

    private static $instance = null;
    private $adapter;

    private function __construct()
    {
        $this->adapter = new ConsultationAdapter();
    }

    /**
     * Retourne l'adapter SQL pour gérer les consultations
     *
     * @return ConsultationAdapter
     */
    public function getAdapter(): ConsultationAdapter
    {
        return $this->adapter;
    }

    /**
     * Crée une consultation en fonction des informations stockées dans la variable globale $_POST et de l'id
     * du patient/usager
     *
     * @param int $patientId
     * @return Consultation
     * @throws Exception si le formulaire(varaible globale $_POST) n'est pas valide
     */
    public function createConsultation(int $patientId): Consultation
    {
        if ($this->hasError()) {
            throw new Exception('Formulaire consultation invalid');
        }

        $consultation = new Consultation(-1, DateTime::createFromFormat("Y-m-d\TH:i", $_POST['date'])->getTimestamp(),
            $_POST['duree'], $patientId, $_POST['medecin'], $_SESSION['user']);

        if ($consultation->getDateRdv() <= time()) {
            throw new Exception('Timestamp plus petit que le temps actuel');
        }

        $this->getAdapter()->insert($consultation);
        return $consultation;
    }

    /**
     * Met à jour une consultation en fonction de son id et de l'id du patient/usager avec les informations stockées
     * dans la variable globale $_POST
     *
     * @param int $patientId
     * @param int $id
     * @throws Exception si le formulaire(varaible globale $_POST) n'est pas valide
     */
    public function updateConsultation(int $patientId, int $id)
    {
        if ($this->hasError()) {
            throw new Exception('Formulaire consultation invalid');
        }

        $timestamp = DateTime::createFromFormat("Y-m-d", $_POST['date'])->getTimestamp() +
            (DateTime::createFromFormat("H:i", $_POST['time'])->getTimestamp() - time());

        if ($timestamp <= time()) {
            throw new Exception('Timestamp plus petit que le temps actuel');
        }

        $consultation = new Consultation($id, $timestamp, $_POST['duree'], $patientId, $_POST['medecin'], $_SESSION['user']);
        $this->getAdapter()->update($consultation);
    }

    /**
     * Retourne true si le formulaire(varaible globale $_POST) est valide
     *
     * @return bool
     */
    private function hasError(): bool
    {
        return (!Utils::isValid(array('date'))
            || (!isset($_POST['duree']) || !is_numeric($_POST['duree']))
            || (!isset($_POST['medecin']) || !is_numeric($_POST['medecin'])));
    }

    /**
     * Retourne l'instance du sigleton ConsultationManager
     *
     * @return ConsultationManager
     */
    public static function getInstance(): ConsultationManager
    {
        if (is_null(ConsultationManager::$instance)) {
            ConsultationManager::$instance = new ConsultationManager();
        }
        return ConsultationManager::$instance;
    }
}