<?php

namespace M3104;

class Autoloader
{

    /**
     * Permet de autoload toutes les classes dans le package class pour éviter de include partout
     * Pour cela cette fonction appelle autoload
     */
    public static function register()
    {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    public static function autoload($class)
    {
        if (strpos($class, __NAMESPACE__ . '\\') === 0) {
            $class = str_replace(__NAMESPACE__ . '\\', '', $class);
            $class = str_replace('\\', '/', $class);
            require 'class/' . $class . '.php';
        }
    }

}