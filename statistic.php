<?php

use M3104\medecin\MedecinManager;
use M3104\statistic\StatisticManager;

require './layout/middleware.php';

$manager = StatisticManager::getInstance();

// Récupère la liste de tous les médecins
$medecins = MedecinManager::getInstance()->getAdapter()->getAll();


// Récupère la liste des statistiques de consultation des médecins
$stats = $manager->getStatisticConsultation();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Statistiques</title>
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>
<body>

<?php require './layout/header.php'; ?>

<!-- Contenue de la page -->
<main>
    <div class="container">
        <div class="funding back content">
            <h1 class="bold minispace">STATISTIQUES</h1>
            <h2 class="minispace">Répartition des usagers selon leur sexe</h2>
            <div class="space">
                <table>
                    <thead>
                        <tr>
                            <th>Tranche d'âge</th>
                            <th>Nombre Hommes</th>
                            <th>Nombre Femmes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--contenue des cellules-->
                        <tr>
                            <td>Moins de 25 ans</td>
                            <?php $statistic = $manager->getStatisticCivility(0, 24); ?>
                            <td><?= is_null($statistic) ? 'Erreur..' : $statistic->getFirst() ?></td>
                            <td><?= is_null($statistic) ? 'Erreur..' : $statistic->getSecond() ?></td>
                        </tr>
                        <tr>
                            <td>Entre 25 et 50 ans</td>
                            <?php $statistic = $manager->getStatisticCivility(25, 50); ?>
                            <td><?= is_null($statistic) ? 'Erreur..' : $statistic->getFirst() ?></td>
                            <td><?= is_null($statistic) ? 'Erreur..' : $statistic->getSecond() ?></td>
                        </tr>
                        <tr>
                            <td>Plus de 50 ans</td>
                            <?php $statistic = $manager->getStatisticCivility(51, 99999); ?>
                            <td><?= is_null($statistic) ? 'Erreur..' : $statistic->getFirst() ?></td>
                            <td><?= is_null($statistic) ? 'Erreur..' : $statistic->getSecond() ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <h2 class="minispace">Information sur les consultations des médecins</h2>
            <table>
                <thead>
                    <tr>
                        <th>Medecin</th>
                        <th>Nombre d'heures de consultations</th>
                        <th>Nombre de consultations</th>
                        <th>Durée moyenne en minutes d'une consultation</th>
                    </tr>
                </thead>
                <tbody>
                    <!--contenue des cellules-->
                    <?php foreach ($stats as $idMed => $statistic): ?>
                        <tr>
                            <td><?= MedecinManager::getInstance()->getMedecin($medecins, $idMed) ?? 'Erreur..' ?></td>
                            <td><?= is_null($statistic) ? 'Erreur..' : round($statistic->getFirst(), 2) ?></td>
                            <td><?= is_null($statistic) ? 'Erreur..' : $statistic->getSecond() ?></td>
                            <td><?= is_null($statistic) ? 'Erreur..' : round($statistic->getThird(), 2) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</main>

<?php require './layout/footer.php'; ?>
</body>
</html>