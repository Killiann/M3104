<?php

use M3104\medecin\MedecinManager;

require './layout/middleware.php';

$manager = MedecinManager::getInstance();

$success = false;
$error = false;

$modify = false;
$id = -1;

// Quand l'utilisateur appuit sur le bouton pour créer un médecin
if (isset($_POST['submit'])) {
    try {
        $medecin = $manager->createMedecin();
        $success = true;
    } catch (Exception $e) {
        $error = true;
    }
}
// Quand l'utilisateur appuit sur le bouton pour supprimer un médecin
elseif (isset($_POST['delete'])) {
    if (is_numeric($id = $_POST['ID'])) {
        $manager->getAdapter()->delete($id);
    }
}
// Quand l'utilisateur appuit sur le bouton pour entrer en mode modification. C'est à dire remplacer les champs de textes
// par des input
elseif (isset($_POST['modify'])) {
    if (is_numeric($id = $_POST['ID'])) {
        $modify = true;
    }
}
// Quand l'utilisateur appuit sur le bouton pour appliquer ces modifitions
elseif (isset($_POST['apply'])) {
    try {
        if (is_numeric($id = $_POST['IDA'])) {
            $manager->updateMedecin($id);
        }
    } catch (Exception $e) {
        echo $e;
    }
}

// Récupère la liste de tous les médecins
$medecins = $manager->getAdapter()->getAll();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Medecin</title>
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>
<body>

<?php require './layout/header.php'; ?>

<!-- Contenue de la page -->
<main>
    <div class="container">
        <div class="back content">
            <h1 class="bold minispace">MÉDECINS</h1>

            <fieldset>
                <legend><h3> Saisie d'un Medecin </h3></legend>
                <form method="POST">
                    <!-- Select de la Civilité -->
                    <div class="br">
                        <label>
                            <select name="civilite">
                                <option value="M">Monsieur</option>
                                <option value="F">Madame</option>
                            </select>
                        </label>
                    </div>

                    <!-- Champ Nom -->
                    <label for="nom">Nom</label>
                    <input type="text" id="nom" name="nom" required>

                    <!-- Champ Prenom -->
                    <label for="prenom">Prenom</label>
                    <input type="text" id="prenom" name="prenom" required>

                    <!-- Affichage de l'erreur ou de la confirmation -->
                    <?php if ($error): ?>
                        <span class="error" aria-live="polite">Erreur durant la création !</span>
                    <?php elseif ($success): ?>
                        <span class="Validate" aria-live="polite">Création effectuée !</span>
                    <?php endif; ?>

                    <input type="submit" id="submit" name="submit" class="creer" value="Créer">
                </form>
            </fieldset>


            <fieldset>
                <legend><h3> Consultation des Medecins Existant</h3></legend>

                <!-- Uniquement si l'utilisateur veut modifier une ligne on créer un formulaire -->
                <?php if ($modify): ?>
                    <form method="POST">
                <?php endif; ?>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Civilité</th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($medecins as $med): ?>
                            <tr>
                                <?php if ($modify && $med->getId() == $id): ?>
                                    <td>
                                        <label>
                                            <select name="civilite">
                                                <option value="M" <?= $med->getCivility() == 'M' ? 'selected' : '' ?>>Monsieur</option>
                                                <option value="F" <?= $med->getCivility() == 'F' ? 'selected' : '' ?>>Madame</option>
                                            </select>
                                        </label>
                                    </td>
                                    <td>
                                        <input type="text" id="nom" name="nom" value="<?= $med->getLastName() ?>" required>
                                    </td>
                                    <td>
                                        <input type="text" id="prenom" name="prenom" value="<?= $med->getName() ?>" required>
                                    </td>
                                    <td>
                                        <label>
                                            <input name="IDA" value="<?= $med->getId() ?>" hidden>
                                        </label>
                                        <input type="submit" id="apply" name="apply" value="Appliquer" class="reduce">
                                    </td>
                                <?php else: ?>
                                    <td><?= $med->getCivilityFormatted() ?></td>
                                    <td><?= $med->getLastName() ?></td>
                                    <td><?= $med->getName() ?></td>
                                    <td>
                                        <form method="POST">
                                            <label>
                                                <input name="ID" value="<?= $med->getId() ?>" hidden>
                                            </label>
                                            <input type="submit" id="modify" name="modify" value="Modifier" class="reduce">
                                            <input type="submit" id="delete" name="delete" value="Supprimer" class="reduce"
                                                   onclick="if (!confirm('Confirmer supression ?')) return false">
                                        </form>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <?php if ($modify): ?>
                    </form>
                <?php endif; ?>
            </fieldset>
        </div>
    </div>
</main>

<?php require './layout/footer.php'; ?>
</body>
</html>
<script type="text/javascript" src="./js/app.js"></script>