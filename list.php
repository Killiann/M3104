<?php

use M3104\patient\PatientManager;

require './layout/middleware.php';

$error = false;
$page = 1;
if (!isset($_GET["page"]) || !is_numeric($_GET["page"])) { // On affiche une erreur si le lien est invalide
    $error = true;
} else {
    // Récup les patients en fonction de la page
    $page = $_GET["page"] < 1 ? 1 : $_GET["page"]; // Math.min ?
    $patients = PatientManager::getInstance()->getAdapter()->getPage($page);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recherche</title>
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>

<body>
<?php require './layout/header.php'; ?>
<!-- Contenue de la page -->
<main>
    <div class="container">
        <div class="back content">
            <h1 class="bold minispace"> Page <?= $page ?> </h1>

            <div class="minispace">
                <!-- Affichage de l'erreur ou la liste des patients -->
                <?php if ($error): ?>
                    <p>Erreur argument de recherche.</p>
                <?php elseif (!empty($patients)): ?>
                    <table class="table">
                        <tbody>
                            <?php foreach ($patients as $index => $patient): ?>
                                <tr>
                                    <td><?= (($index + 1) * $page) ?></td>
                                    <td><?= $patient ?></td>
                                    <td>
                                        <form action="user" method="GET">
                                            <label>
                                                <input name="ID" value="<?= $patient->getId() ?>" hidden>
                                            </label>
                                            <input type='submit' value='Selectionner' class="reduce">
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <p>Aucun résultats...</p>
                <?php endif; ?>
            </div>

            <div class="page">
                <!-- Affichage de la page précédente uniquement si on peut revenir en arrière -->
                <?php if ($page > 1): ?>
                    <div class="column">
                        <form action="list" method="GET">
                            <label>
                                <input name="page" value="<?= $page-1 ?>" hidden>
                            </label>
                            <input type="submit" value="Page précédente">
                        </form>
                    </div>
                <?php endif; ?>

                <!-- Affichage de la page suivante uniquement si on peut continuer -->
                <?php if (!empty($patients) && sizeof($patients) >= 10): ?>
                    <div class="column right">
                        <form action="list" method="GET">
                            <label>
                                <input name="page" value="<?= $page+1 ?>" hidden>
                            </label>
                            <input type="submit" value="Page suivante">
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>
<?php require './layout/footer.php'; ?>
</body>
</html>