<!-- Ce fichier contient le header utilisé dans toutes les pages du site -->
<body>
<!-- Création des flèches -->
<div class="arrow">
    <a href="#arrowup"><img src="./images/arrowup.png" alt="Retour haut de page" title="Cliquez pour remonter"/></a>
    <a href="#arrowdown"><img src="./images/arrowdown.png" alt="Retour bas de page" title="Cliquez pour descendre"/></a>
</div>

<!-- Header & ancre arrowup -->
<header id="arrowup">
    <div class="container">

        <!-- Création Nav bar -->
        <nav>
            <ul>
                <li><a href="index" class="bold">Usagers</a></li>
                <li><a href="medecin" class="bold">Médecins</a></li>
                <li><a href="planning" class="bold">Consultations</a></li>
                <li><a href="statistic" class="blue bold">Statistiques</a></li>
            </ul>
            <!--pour le bouton deconnexion a droite .. -->
            <p><a href="disconnection" class="bold" id="logout-button">Déconnexion</a></p>
        </nav>
    </div>
</header>