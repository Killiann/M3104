<!-- Ce fichier contient le footer utilisé dans toutes les pages du site -->
<footer id="arrowdown">
    <div class="container">

        <!-- Création des 3 colonnes -->
        <div class="column">
            <a target="_blank" href="https://iut-informatique.univ-tlse3.fr">
                <img src="./images/logo_iut_info.png" alt="Logo de l'iut informatique">
            </a>
        </div>
        <div class="column right">
            <p><a class="maj">Réalisé par</a></p>

            <!-- Création d'une liste -->
            <ul>
                <li><p>Killian FALGUIERE</p></li>
                <li><p>Vincent FERNANDEZ</p></li>
            </ul>
        </div>
        <div class="column right">
            <p><a class="maj">Gestion Medicale</a></p>

            <!-- Création d'une liste -->
            <ul>
                <li><a href="index">Usagers</a></li>
                <li><a href="medecin">Médecins</a></li>
                <li><a href="planning">Consultations</a></li>
                <li><a href="statistic">Statistiques</a></li>
            </ul>
        </div>
    </div>
</footer>