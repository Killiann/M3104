<?php

use M3104\util\Utils;

session_start();

// Si la personne qui veut accéder au site n'est pas connecté elle est automatiquement redirigé vers la page de connexion
if (!isset($_SESSION['user'])) {
    header('Location: ./connection');
    return;
}

// Load automatiquement toutes les classes
require './class/Autoloader.php';
M3104\Autoloader::register();

// Permet d'éviter les injections
Utils::serialize();