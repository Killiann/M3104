// Permet d'éviter de recréer un usager à chaque reload de la page (POST uniquement)
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}

// Quand l'utilisateur arrive sur la page on le remet à sa dernière localisation
document.addEventListener("DOMContentLoaded", () => {
    let scrollpos = localStorage.getItem('scrollpos');
    if (scrollpos) window.scrollTo(0, scrollpos);
});

// On sauvegarde la position de l'utilisateur quand il ferme la page
window.onbeforeunload = () => localStorage.setItem('scrollpos', window.scrollY);