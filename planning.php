<?php

use M3104\consultation\ConsultationManager;
use M3104\medecin\MedecinManager;
use M3104\patient\PatientManager;

require './layout/middleware.php';

// Récupère la liste de tous les médecins
$medecins = MedecinManager::getInstance()->getAdapter()->getAll();
$patients = PatientManager::getInstance()->getAdapter()->getAll();

$consultationSuccess = false;
$consultationError = false;
$planning = false;
$idMedecin = -1;

// Quand l'utilisateur appuit sur le bouton Afficher pour le planning
if (isset($_POST['submit'])) {
    $planning = true;
    $idMedecin = $_POST['medecin'] ?? -1; // Si isset($_POST['medecin']) alors $_POST['medecin'] sinon -1
    $consultations = ConsultationManager::getInstance()->getAdapter()->getAllOrMedecin($idMedecin, $_POST['datePlanning']);
}
//Quand l'utilisateur appuit sur le boutton créer pour créer une consultation
elseif (isset($_POST['submitCon'])) {
    try {
        $consultation = ConsultationManager::getInstance()->createConsultation($_POST["patient"]);
        $consultationSuccess = true;
    } catch (Exception $e) { // Si exception on affiche une erreur
        $consultationError = true;
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consultations</title>
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="app.css">
</head>
<body>

<?php require './layout/header.php'; ?>
<!-- Contenue de la page -->
<main>
    <div class="container">
        <div class="back content">
            <div class="space">
                <h1 class="bold">CONSULTATIONS</h1>

                <fieldset>
                    <legend><h3> Créer une Consultation </h3></legend>
                    <form method="POST">
                        <label for="patient">Patient</label>
                        <select id="patient" name="patient">
                            <?php foreach ($patients as $patient): ?>
                                <option value="<?= $patient->getId() ?>"><?= $patient ?></option>
                            <?php endforeach; ?>
                        </select>

                        <div class="br">
                            <label for="medecin">Medecin</label>
                            <select id="medecin" name="medecin">
                                <?php foreach ($medecins as $medecin): ?>
                                    <option value="<?= $medecin->getId() ?>"><?= $medecin ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="br">
                            <!-- Champ date  -->
                            <label for="date">Date consultation : </label>
                            <input type="datetime-local" id="date" name="date">
                        </div>

                        <label for="duree">Durée (minutes)</label>
                        <input type="text" id="duree" name="duree" value="30" required>

                        <?php if ($consultationError): ?>
                            <span class="error" aria-live="polite">Erreur durant la création: Vérifiez vos informations !</span>
                        <?php elseif ($consultationSuccess): ?>
                            <span class="Validate" aria-live="polite">Création effectuée !</span>
                        <?php endif; ?>

                        <input type="submit" id="submit" name="submitCon" value="Créer">
                    </form>
                </fieldset>
            </div>

            <h1 class="bold">PLANNING</h1>
            <p class="minispace">Si vous voulez créer, modifier ou supprimer une consultation vous devez aller sur le profil de l'usager en question. Vous pouvez utiliser un des boutons<strong>'Sélectionner'</strong> ci-dessous pour y accéder plus rapidement.</p>
            <form method="POST" class="space">
                <div class="br">
                    <label for="bday">Veuillez saisir une date :</label>
                    <input type="date" id="date" name="datePlanning" value="<?= $_POST['datePlanning'] ?? date('Y-m-d', time()) ?>">
                </div>

                <label for="medecin">Médecin (Optionnel) :</label>
                <select id="medecin" name="medecin">
                    <option value="-1">Aucun</option>
                    <?php foreach ($medecins as $medecin): ?>
                        <option value="<?= $medecin->getId() ?>" <?= $medecin->getId() == $idMedecin ? 'selected' : ''?>><?= $medecin ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" name="submit" value="Afficher">
            </form>

            <!-- Afficher le planning -->
            <?php if ($planning): ?>
                <?php if (empty($consultations)): ?>
                    <p class="center">Aucune consultations...</p>
                <?php endif; ?>
                <?php foreach ($consultations as $key => $value):?>
                    <div class="space">
                        <h2 class="minispace">Planning
                            de <?= MedecinManager::getInstance()->getAdapter()->get($key) ?></h2>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Heure</th>
                                    <th>Durée (minutes)</th>
                                    <th>Usager/Patient</th>
                                    <th>Secrétaire</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($value as $consul): ?>
                                    <tr>
                                        <td><?= date("H:i", $consul->getDateRdv()) ?></td>
                                        <td><?= $consul->getDuree() ?></td>
                                        <td><?= $consul->getPatient() ?></td>
                                        <td><?= $consul->getSecretaire() ?></td>
                                        <td>
                                            <form action="user" method="GET" name="formSelect">
                                                <label>
                                                    <input name="ID" value="<?= $consul->getIdPatient() ?>" hidden>
                                                    <input name="idCon" value="<?= $consul->getId() ?>" hidden>
                                                </label>
                                                <input type="submit" value="Sélectionner">
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endforeach;
            endif; ?>
        </div>
    </div>
</main>
<?php require './layout/footer.php'; ?>
</body>
</html>
<script type="text/javascript" src="./js/app.js"></script>