--
-- Base de données : `projetweb`
--

-- --------------------------------------------------------

--
-- Structure de la table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation`
(
    `id_consultation` int(11) NOT NULL AUTO_INCREMENT,
    `date_rdv`        int(11) DEFAULT NULL,
    `duree`           int(11) DEFAULT NULL,
    `id_medecin`      int(11) DEFAULT NULL,
    `id_patient`      int(11) DEFAULT NULL,
    `id_secretaire`   int(11) DEFAULT NULL,
    PRIMARY KEY (`id_consultation`),
    FOREIGN KEY (`id_medecin`) REFERENCES medecin (`id_medecin`) ON DELETE CASCADE,
    FOREIGN KEY (`id_patient`) REFERENCES patient (`id_patient`) ON DELETE CASCADE,
    KEY `id_secretaire` (`id_secretaire`)
)
    DEFAULT CHARSET = latin1;


--
-- Structure de la table `medecin`
--

DROP TABLE IF EXISTS `medecin`;
CREATE TABLE IF NOT EXISTS `medecin`
(
    `id_medecin` int(11)     NOT NULL AUTO_INCREMENT,
    `civilite`   char(1)     NOT NULL DEFAULT 'M',
    `prenom`     varchar(50) NOT NULL,
    `nom`        varchar(50) NOT NULL,
    PRIMARY KEY (`id_medecin`)
)
    DEFAULT CHARSET = latin1;


--
-- Structure de la table `patient`
--

DROP TABLE IF EXISTS `patient`;
CREATE TABLE IF NOT EXISTS `patient`
(
    `id_patient`     int(11)      NOT NULL AUTO_INCREMENT,
    `civilite`       char(1)      NOT NULL,
    `nom`            varchar(50)  NOT NULL,
    `prenom`         varchar(50)  NOT NULL,
    `adresse`        varchar(255) NOT NULL,
    `date_naissance` int(11)      NOT NULL,
    `lieu_naissance` varchar(255) NOT NULL,
    `secu_sociale`   varchar(15)  NOT NULL,
    `id_medecin`     int(11) DEFAULT NULL,
    PRIMARY KEY (`id_patient`),
    FOREIGN KEY (`id_medecin`) REFERENCES medecin (`id_medecin`) ON DELETE SET NULL
)
    DEFAULT CHARSET = latin1;

-- --------------------------------------------------------

--
-- Structure de la table `secretaire`
--

DROP TABLE IF EXISTS `secretaire`;
CREATE TABLE IF NOT EXISTS `secretaire`
(
    `id_secretaire` int(11)      NOT NULL AUTO_INCREMENT,
    `utilisateur`   varchar(50)  NOT NULL,
    `motdepasse`    varchar(255) NOT NULL,
    PRIMARY KEY (`id_secretaire`)
)
    DEFAULT CHARSET = latin1;

-- User: Killian et MDP: azerty
INSERT INTO `secretaire` (`id_secretaire`, `utilisateur`, `motdepasse`) VALUES
    (1, 'Killian', 'ab4f63f9ac65152575886860dde480a1');